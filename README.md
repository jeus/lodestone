# Lodestone
Magnet stone for navigating your vehicle to nearest electric charging station


## Install
For further reference, please consider the following sections:

* running service
  1) running docker-compose.yml
  ```
  ~# docker-compose up 
  ```
  2) insert data in resources/data/sql/sample_data.sql

### How To Build
```
~# gradle clean build 
```

### How To Run
you can run project directly by `java` command or handle it by docker image both of them run at same port `8086`


* [Open API](http://localhost:8086/swagger-ui/index.html)

* if you have `Intellij Idea`, you can run http requests `lodestone/lodestone.http` 


### Additional Links
These additional references should also help you:

* [MapStruct](https://mapstruct.org) Java bean mappings, the easy way!
* [Lombok](https://projectlombok.org) is a java library that automatically plugs into your editor and build tools, spicing up your java
* [ltree](https://www.postgresql.org/docs/current/ltree.html#id-1.11.7.30.6) postgres module
* [PostGis](https://postgis.net)

