package se.dzm.lodestone.core.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import se.dzm.lodestone.core.dao.CompanyRepository;
import se.dzm.lodestone.core.dao.JpaCompanyDao;
import se.dzm.lodestone.core.pojo.model.sql.Company;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;

/**
 * @author alikhandani
 * @created 21/06/2022
 * @project lodestone
 */
@SpringBootTest
class CompanyServiceImplTest {

    @MockBean
    CompanyRepository companyRepository;

    @MockBean
    JpaCompanyDao jpaCompanyDao;

    @Autowired
    CompanyServiceImpl companyService;


    @Test
    void saveCompany() {
        Company company = new Company(201, 101, "COMPANY_A", "101");
        Mockito.when(jpaCompanyDao.findCompanyById(company.getId())).thenReturn(mockmCompanyAbsent());
        Mockito.when(jpaCompanyDao.findCompanyById(company.getParentId())).thenReturn(mockmCompany());
        Mockito.when(jpaCompanyDao.saveAndFlush(company)).thenAnswer(i -> i.getArguments()[0]);

        var result = companyService.saveCompany(company);
        assertEquals(result.getHierarchy(), "101.201");

    }


    @Test
    void saveCompanyAlreadyExist() {
        Mockito.when(jpaCompanyDao.findCompanyById(anyInt())).thenReturn(mockmCompany());
        Company company = new Company(101, null, "COMPANY_A", "101");
        Mockito.when(jpaCompanyDao.saveAndFlush(company)).thenAnswer(i -> i.getArguments()[0]);

        Exception exception = assertThrows(RuntimeException.class, () -> {
            companyService.saveCompany(company);
        });

        String expectedMessage = "company id 101 already exist. change company id";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void saveCompanyNull() {
        Mockito.when(jpaCompanyDao.findCompanyById(anyInt())).thenReturn(mockmCompany());

        Exception exception = assertThrows(RuntimeException.class, () -> {
            companyService.saveCompany(null);
        });

        String expectedMessage = "Company is absent";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    void saveCompanyIdNull() {
        Mockito.when(jpaCompanyDao.findCompanyById(anyInt())).thenReturn(mockmCompany());
        Company company = new Company(null, null, "COMPANY_A", "101");
        Exception exception = assertThrows(RuntimeException.class, () -> {
            companyService.saveCompany(company);
        });

        String expectedMessage = "Company Id is absent";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

    }


    @Test
    void saveCompanyNameNull() {
        Mockito.when(jpaCompanyDao.findCompanyById(anyInt())).thenReturn(mockmCompany());
        Company company = new Company(101, null, null, "101");
        Exception exception = assertThrows(RuntimeException.class, () -> {
            companyService.saveCompany(company);
        });

        String expectedMessage = "Company name is absent";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    void saveCompanyParenAbsent() {
        Mockito.when(jpaCompanyDao.findCompanyById(anyInt())).thenReturn(mockmCompanyAbsent());
        Company company = new Company(101, 200, "COMPANY1", "101");
        Mockito.when(jpaCompanyDao.saveAndFlush(company)).thenReturn(null);
        Exception exception = assertThrows(RuntimeException.class, () -> {
            companyService.saveCompany(company);
        });

        String expectedMessage = "Parent company id 200 is not valid. This company is not exist";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    private Optional<Company> mockmCompany() {
        Company company = new Company(101, null, "COMPANY_A", "101");
        return Optional.of(company);
    }

    private Optional<Company> mockmCompanyAbsent() {
        Optional<Company> company = Optional.empty();
        return company;
    }


}