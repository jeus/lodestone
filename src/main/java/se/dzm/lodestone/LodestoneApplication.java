package se.dzm.lodestone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class LodestoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(LodestoneApplication.class, args);
	}

}
