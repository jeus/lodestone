package se.dzm.lodestone.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.dzm.lodestone.common.exception.IncorrectParameterException;
import se.dzm.lodestone.common.helper.AssertHelper;
import se.dzm.lodestone.common.helper.Validator;
import se.dzm.lodestone.core.dao.JpaCompanyDao;
import se.dzm.lodestone.core.pojo.model.sql.Company;

import java.util.Optional;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */

@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    JpaCompanyDao jpaCompanyDao;
    @Autowired
    Validator validator;
    @Autowired
    AssertHelper assertHelper;

    @Override
    @Transactional
    public Company saveCompany(Company company) {
        checkCompany(company);
        var hierarchy = company.getParentId() == null ?
                company.getId() + "" : getHierarchy(company.getParentId(), company.getId());
        company.setHierarchy(hierarchy);
        return jpaCompanyDao.saveAndFlush(company);
    }

    private void checkCompany(Company company) {
        validator.validateCompany(company);
        Optional<Company> company1 = jpaCompanyDao.findCompanyById(company.getId());
        if (company1.isPresent())
            throw new IncorrectParameterException("company id " + company.getId() + " already exist. change company id");

    }

    private String getHierarchy(int parentId, int id) {
        return getParentHierarchy(parentId) + "." + id;
    }


    private String getParentHierarchy(int parentId) {
        Optional<Company> parentCompany = jpaCompanyDao.findCompanyById(parentId);
        if (parentCompany.isEmpty())
            throw new IncorrectParameterException("Parent company id " + parentId + " is not valid. This company is not exist");

        return parentCompany.get().getHierarchy();
    }
}
