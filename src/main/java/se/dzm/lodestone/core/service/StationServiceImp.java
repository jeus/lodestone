package se.dzm.lodestone.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.dzm.lodestone.common.exception.IncorrectParameterException;
import se.dzm.lodestone.common.helper.AssertHelper;
import se.dzm.lodestone.common.helper.Validator;
import se.dzm.lodestone.core.dao.JpaCompanyDao;
import se.dzm.lodestone.core.dao.JpaStationDao;
import se.dzm.lodestone.core.pojo.model.sql.Company;
import se.dzm.lodestone.core.pojo.model.sql.Station;

import java.util.List;
import java.util.Optional;

/**
 * @author alikhandani
 * @created 21/06/2022
 * @project lodestone
 */
@Service
public class StationServiceImp implements StationService {

    @Autowired
    JpaStationDao jpaStationDao;
    @Autowired
    JpaCompanyDao jpaCompanyDao;
    @Autowired
    Validator validator;
    @Autowired
    AssertHelper assertHelper;


    @Override
    public Station saveStation(Station station) {
        checkStation(station);
        return jpaStationDao.saveAndFlush(station);
    }

    public List<Station> findNearestStation(Integer companyId, Double lat, Double lon, int radius) {
        validator.validateFindNearest(companyId, lat, lon);
        Optional<Company> company = jpaCompanyDao.findCompanyById(companyId);
        if (company.isEmpty())
            throw new IncorrectParameterException("company id " + companyId + " is not valid. This company is not exist");

       return jpaStationDao.findNearestStations(company.get().getHierarchy(), lat, lon, radius);

    }


    private void checkStation(Station station) {
        validator.validateStation(station);
        Optional<Station> station1 = jpaStationDao.findStationById(station.getId());
        if (station1.isPresent())
            throw new IncorrectParameterException("station id " + station.getId() + " already exist. change station id");
        Optional<Company> company = jpaCompanyDao.findCompanyById(station.getCompanyId());
        if (company.isEmpty())
            throw new IncorrectParameterException("company id " + station.getCompanyId() + " is not valid. This company is not exist");

    }


}
