package se.dzm.lodestone.core.service;

import se.dzm.lodestone.core.pojo.model.sql.Company;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */
public interface CompanyService {

    Company saveCompany(Company company);

}
