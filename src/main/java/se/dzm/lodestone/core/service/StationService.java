package se.dzm.lodestone.core.service;

import se.dzm.lodestone.core.pojo.model.sql.Station;

import java.util.List;

/**
 * @author alikhandani
 * @created 21/06/2022
 * @project lodestone
 */
public interface StationService {

    Station saveStation(Station station);

    List<Station> findNearestStation(Integer companyId, Double lat, Double lon,int radius);
}
