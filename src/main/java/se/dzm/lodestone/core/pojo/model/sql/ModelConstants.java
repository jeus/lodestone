package se.dzm.lodestone.core.pojo.model.sql;

public class ModelConstants {

    private ModelConstants() {
    }

    /**
     * Company
     */
    public static final String COMPANY_COLUMN_FAMILY_NAME = "company";
    public static final String COMPANY_ID = "id";
    public static final String COMPANY_NAME = "name";
    public static final String COMPANY_PARENT_ID = "parent_company_id";
    public static final String COMPANY_HIERARCHY = "hierarchy";
    /**
     * Station
     */
    public static final String STATION_COLUMN_FAMILY_NAME = "station";
    public static final String STATION_ID = "id";
    public static final String STATION_NAME = "name";
    public static final String STATION_STATE = "state";
    public static final String STATION_COMPANY_ID = "company_id";
    public static final String STATION_LATITUDE = "latitude";
    public static final String STATION_LONGITUDE = "longitude";


}
