package se.dzm.lodestone.core.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author alikhandani
 * @created 21/06/2022
 * @project lodestone
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StationDto {

    private Integer id;
    private String name;
    private String state;
    private Integer companyId;
    private Double latitude;
    private Double longitude;
}
