package se.dzm.lodestone.core.pojo.dto.translator;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import se.dzm.lodestone.core.pojo.dto.StationDto;
import se.dzm.lodestone.core.pojo.model.sql.Station;

import java.util.List;

/**
 * @author alikhandani
 * @created 21/06/2022
 * @project lodestone
 */
@Mapper(componentModel = "spring")
public interface StationMapper {

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "name", source = "name"),
            @Mapping(target = "companyId", source = "companyId"),
            @Mapping(target = "latitude", source = "latitude"),
            @Mapping(target = "longitude", source = "longitude"),
            @Mapping(target = "state", source = "state")
    })
    StationDto toStationDto(Station station);

    Station toStation(StationDto stationDto);

    List<StationDto> toStationDtos(List<Station> station);
}
