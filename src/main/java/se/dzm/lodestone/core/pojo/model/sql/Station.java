package se.dzm.lodestone.core.pojo.model.sql;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author alikhandani
 * @created 21/06/2022
 * @project lodestone
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = ModelConstants.STATION_COLUMN_FAMILY_NAME)
public class Station {

    @Id
    @Column(name = ModelConstants.STATION_ID, nullable = false)
    private Integer id;
    @Column(name = ModelConstants.STATION_NAME, nullable = false)
    private String name;
    @Column(name = ModelConstants.STATION_STATE)
    private String state;
    @Column(name = ModelConstants.STATION_COMPANY_ID)
    private Integer companyId;
    @Column(name = ModelConstants.STATION_LATITUDE, nullable = false)
    private Double latitude;
    @Column(name = ModelConstants.STATION_LONGITUDE, nullable = false)
    private Double longitude;

}