package se.dzm.lodestone.core.pojo.dto.translator;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import se.dzm.lodestone.core.pojo.dto.CompanyDto;
import se.dzm.lodestone.core.pojo.model.sql.Company;

import java.util.List;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */
@Mapper(componentModel = "spring")
public interface CompanyMapper {

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "name", source = "name"),
            @Mapping(target = "parentId", source = "parentId"),
    })
    CompanyDto toCompanyDto(Company company);

    List<CompanyDto> toCompanyDto(List<Company> movie);

    @Mapping(target = "hierarchy", ignore = true)
    Company toCompany(CompanyDto companyDto);

    List<Company> toCompany(List<CompanyDto> companyDto);


}
