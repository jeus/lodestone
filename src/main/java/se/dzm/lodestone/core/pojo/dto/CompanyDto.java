package se.dzm.lodestone.core.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDto {
    private Integer id;
    private Integer parentId;
    private String name;
}
