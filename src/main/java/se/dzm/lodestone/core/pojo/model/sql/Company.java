package se.dzm.lodestone.core.pojo.model.sql;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author alikhandani
 * @created 18/06/2022
 * @project lodestone
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = ModelConstants.COMPANY_COLUMN_FAMILY_NAME)
public class Company {

    @Id
    @Column(name = ModelConstants.COMPANY_ID, nullable = false)
    private Integer id;
    @Column(name = ModelConstants.COMPANY_PARENT_ID)
    private Integer parentId;
    @Column(name = ModelConstants.COMPANY_NAME, nullable = false)
    private String name;
    @Column(name = ModelConstants.COMPANY_HIERARCHY, nullable = false, columnDefinition = "ltree")
    @Type(type = "se.dzm.lodestone.core.pojo.model.sql.type.LTreeType")
    private String hierarchy;

}
