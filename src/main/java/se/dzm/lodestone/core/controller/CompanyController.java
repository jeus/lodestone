package se.dzm.lodestone.core.controller;

import com.sun.istack.NotNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.dzm.lodestone.common.exception.LodestoneException;
import se.dzm.lodestone.core.pojo.dto.CompanyDto;
import se.dzm.lodestone.core.pojo.dto.translator.CompanyMapper;
import se.dzm.lodestone.core.service.CompanyService;

/**
 * @author alikhandani
 * @created 18/06/2022
 * @project lodestone
 */

@RestController
@RequestMapping("/api")
public class CompanyController extends BaseController {

    final CompanyMapper companyMapper;
    final CompanyService companyService;

    public CompanyController(CompanyMapper companyMapper, CompanyService companyService) {
        this.companyMapper = companyMapper;
        this.companyService = companyService;
    }

    @PostMapping(path = "/company")
    public CompanyDto addNewCompany(@NotNull @RequestBody CompanyDto companyDto) throws LodestoneException {
        try {
            var result = companyService.saveCompany(companyMapper.toCompany(companyDto));
            return companyMapper.toCompanyDto(result);
        } catch (Exception exception) {
            throw handleException(exception);
        }
    }

}
