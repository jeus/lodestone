package se.dzm.lodestone.core.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import se.dzm.lodestone.common.exception.*;

import java.util.concurrent.ExecutionException;

/**
 * @author alikhandani
 * @created 20/06/2022
 * @project lodestone
 */
@Slf4j
public abstract class BaseController {
    @Autowired
    private LodestoneResponseHandler errorResponseHandler;

    @ExceptionHandler(value = {LodestoneException.class})
    public ResponseEntity<ErrorMessage> handleThingsboardException(LodestoneException ex) {
        return errorResponseHandler.handle(ex);
    }


    protected LodestoneException handleException(Exception exception) {
        return handleException(exception, true);
    }


    private LodestoneException handleException(Throwable exception, boolean logException) {
        if (exception instanceof ExecutionException) {
            exception = exception.getCause();
        }
        String cause = "";
        if (exception.getCause() != null) {
            cause = exception.getCause().getClass().getCanonicalName();
        }
        if (exception instanceof LodestoneException) {
            return (LodestoneException) exception;
        }
        if (exception instanceof IllegalArgumentException || exception instanceof IncorrectParameterException
                || exception instanceof DataValidationException || cause.contains("IncorrectParameterException")) {
            return new LodestoneException(exception.getMessage(), LodestoneErrorCode.BAD_REQUEST_PARAMS);
        }
        return new LodestoneException(exception.getMessage(), LodestoneErrorCode.BAD_REQUEST_PARAMS);
    }

}
