package se.dzm.lodestone.core.controller;

import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import se.dzm.lodestone.common.exception.LodestoneException;
import se.dzm.lodestone.core.pojo.dto.StationDto;
import se.dzm.lodestone.core.pojo.dto.translator.StationMapper;
import se.dzm.lodestone.core.service.StationService;

import java.util.List;


/**
 * @author alikhandani
 * @created 18/06/2022
 * @project lodestone
 */

@RestController
@RequestMapping("/api")
@Slf4j
public class StationController extends BaseController {

    @Autowired
    StationMapper stationMapper;
    @Autowired
    StationService stationService;

    @PostMapping(path = "/station")
    public StationDto importAllCompany(@NotNull @RequestBody StationDto stationDto) throws LodestoneException {
        try {
            var result = stationService.saveStation(stationMapper.toStation(stationDto));
            return stationMapper.toStationDto(result);
        } catch (Exception exception) {
            throw handleException(exception);
        }
    }

    @GetMapping(path = "/station/nearest")
    public List<StationDto> importAllCompany(@RequestParam(name = "lat") Double latitude,
                                             @RequestParam(name = "lon") Double longitude,
                                             @RequestParam(name = "company") Integer company,
                                             @RequestParam(name = "radius") Integer radius) throws LodestoneException {
        try {
            var result = stationService.findNearestStation(company, latitude, longitude, radius);
            return stationMapper.toStationDtos(result);
        } catch (Exception exception) {
            throw handleException(exception);
        }
    }

}

