package se.dzm.lodestone.core.dao;

import se.dzm.lodestone.core.pojo.model.sql.Company;

import java.util.Optional;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */
public interface CompanyDao {

    Company saveAndFlush(Company company);

    Optional<Company> findCompanyById(Integer id);
}
