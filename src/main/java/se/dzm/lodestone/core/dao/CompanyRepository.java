package se.dzm.lodestone.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import se.dzm.lodestone.core.pojo.model.sql.Company;

import java.util.Optional;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */

public interface CompanyRepository extends JpaRepository<Company, Integer> {

    @Query("SELECT a FROM Company a WHERE a.id = :id")
    Optional<Company> findCompanyById(@Param("id") Integer id);
}
