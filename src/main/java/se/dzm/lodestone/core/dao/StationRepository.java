package se.dzm.lodestone.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import se.dzm.lodestone.core.pojo.model.sql.Station;

import java.util.List;
import java.util.Optional;

/**
 * @author alikhandani
 * @created 21/06/2022
 * @project lodestone
 */
public interface StationRepository extends JpaRepository<Station, Integer> {

    @Query("SELECT a FROM Station a WHERE a.id = :id")
    Optional<Station> findStationById(@Param("id") Integer id);


    @Query(value = "SELECT * FROM station s WHERE ST_DWithin(ST_MakePoint(s.latitude,s.longitude)\\:\\:geography, ST_MakePoint(:lat,:lon)\\:\\:geography, :radius) and s.company_id in (SELECT id FROM company WHERE hierarchy <@ CAST(:hierarchy AS ltree)) ORDER BY ST_MakePoint(s.latitude,s.longitude)\\:\\:geography <-> ST_MakePoint(:lat,:lon)\\:\\:geography;", nativeQuery = true)
    List<Station> findNearestStations(@Param("hierarchy") String hierarchy,
                                     @Param("lat") Double lat,
                                     @Param("lon") Double lon,
                                     @Param("radius") Integer radius);
}
