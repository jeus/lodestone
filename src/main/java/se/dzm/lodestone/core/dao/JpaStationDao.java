package se.dzm.lodestone.core.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.dzm.lodestone.core.pojo.model.sql.Station;

import java.util.List;
import java.util.Optional;

/**
 * @author alikhandani
 * @created 21/06/2022
 * @project lodestone
 */
@Component
public class JpaStationDao implements StationDao {

    @Autowired
    StationRepository stationRepository;

    @Override
    public Station saveAndFlush(Station station) {
        return stationRepository.saveAndFlush(station);
    }

    @Override
    public Optional<Station> findStationById(Integer id) {
        return stationRepository.findStationById(id);
    }

    @Override
    public List<Station> findNearestStations(String hierarchy, Double lat, Double lon, int radius) {
        return stationRepository.findNearestStations(hierarchy, lat, lon, radius);
    }
}
