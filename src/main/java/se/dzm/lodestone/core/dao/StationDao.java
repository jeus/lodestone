package se.dzm.lodestone.core.dao;

import se.dzm.lodestone.core.pojo.model.sql.Station;

import java.util.List;
import java.util.Optional;

/**
 * @author alikhandani
 * @created 21/06/2022
 * @project lodestone
 */
public interface StationDao {
    Station saveAndFlush(Station station);

    Optional<Station> findStationById(Integer id);

    List<Station> findNearestStations(String hierarchy, Double lat, Double lon, int distance);
}
