package se.dzm.lodestone.core.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.dzm.lodestone.core.pojo.model.sql.Company;

import java.util.Optional;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */
@Component
public class JpaCompanyDao implements CompanyDao{

    @Autowired
    CompanyRepository companyRepository;

    @Override
    public Company saveAndFlush(Company company) {
       return companyRepository.saveAndFlush(company);
    }

    @Override
    public Optional<Company> findCompanyById(Integer id) {
        return companyRepository.findCompanyById(id);
    }
}
