package se.dzm.lodestone.common.helper;

import org.springframework.stereotype.Component;
import se.dzm.lodestone.common.exception.DataValidationException;
import se.dzm.lodestone.core.pojo.model.sql.Company;
import se.dzm.lodestone.core.pojo.model.sql.Station;

/**
 * @author alikhandani
 * @created 02/06/2020
 * @project lunatech
 */
@Component
public class Validator {

    protected final AssertHelper assertHelper;

    public Validator(AssertHelper assertHelper) {
        this.assertHelper = assertHelper;
    }

    public void validateCompany(Company company) {
        try {
            assertHelper.notNull(company);
        } catch (Exception ex) {
            throw new DataValidationException("Company is absent", ex);
        }
        try {
            assertHelper.notNull(company.getId());
        } catch (Exception ex) {
            throw new DataValidationException("Company Id is absent", ex);
        }
        try {
            assertHelper.notEmpty(company.getName());
        } catch (Exception ex) {
            throw new DataValidationException("Company name is absent", ex);
        }
    }

    public void validateStation(Station station) {
        try {
            assertHelper.notNull(station);
        } catch (Exception ex) {
            throw new DataValidationException("Station is absent", ex);
        }
        try {
            assertHelper.notNull(station.getId());
        } catch (Exception ex) {
            throw new DataValidationException("Station Id is absent", ex);
        }
        try {
            assertHelper.notNull(station.getCompanyId());
        } catch (Exception ex) {
            throw new DataValidationException("Company Id is absent", ex);
        }
        try {
            assertHelper.notEmpty(station.getName());
        } catch (Exception ex) {
            throw new DataValidationException("Station name is absent", ex);
        }
        try {
            assertHelper.notNull(station.getLatitude());
        } catch (Exception ex) {
            throw new DataValidationException("Latitude is absent", ex);
        }
        try {
            assertHelper.notNull(station.getLongitude());
        } catch (Exception ex) {
            throw new DataValidationException("Longitude is absent", ex);
        }
    }

    public void validateFindNearest(Integer companyId, Double latitude, Double longitude) {
        try {
            assertHelper.notNull(companyId);
        } catch (Exception ex) {
            throw new DataValidationException("company id is absent", ex);
        }
        try {
            assertHelper.notNull(latitude);
        } catch (Exception ex) {
            throw new DataValidationException("latitude is requirement", ex);
        }
        try {
            assertHelper.notNull(longitude);
        } catch (Exception ex) {
            throw new DataValidationException("longitude is absent", ex);
        }

    }

}
