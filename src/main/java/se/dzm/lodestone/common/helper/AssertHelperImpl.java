package se.dzm.lodestone.common.helper;

import org.springframework.stereotype.Component;
import se.dzm.lodestone.common.exception.AssertingException;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */
@Component
public class AssertHelperImpl implements AssertHelper {

    @Override
    public void notNull(String str) throws AssertingException {
        if (str == null) {
            throw new AssertingException("String is Null");
        }
    }

    @Override
    public void notNull(Object obj) throws AssertingException {
        if (obj == null) {
            throw new AssertingException("Object is Null");
        }
    }

    @Override
    public void notEmpty(String str) throws AssertingException {
        if (str.isEmpty()) {
            throw new AssertingException("String is Empty");
        }
    }


}
