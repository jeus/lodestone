package se.dzm.lodestone.common.helper;

import org.springframework.stereotype.Component;
import se.dzm.lodestone.common.exception.AssertingException;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */

@Component
public interface AssertHelper {

    void notNull(String str) throws AssertingException;
    void notNull(Object obj) throws AssertingException;
    void notEmpty(String str) throws AssertingException;

}