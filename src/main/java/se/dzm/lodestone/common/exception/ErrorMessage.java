package se.dzm.lodestone.common.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;


import java.util.Date;

/**
 * @author alikhandani
 * @created 20/06/2022
 * @project lodestone
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
  private HttpStatus statusCode;
  private Date timestamp;
  private String message;
  private String description;

}