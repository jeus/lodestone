package se.dzm.lodestone.common.exception;
/**
 * @author alikhandani
 * @created 20/06/2022
 * @project lodestone
 */
public class IncorrectParameterException extends RuntimeException {

    private static final long serialVersionUID = 601995650578985289L;

    public IncorrectParameterException(String message) {
        super(message);
    }

}
