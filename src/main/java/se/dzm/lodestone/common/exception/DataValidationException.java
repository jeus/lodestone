package se.dzm.lodestone.common.exception;

/**
 * @author alikhandani
 * @created 20/06/2022
 * @project lodestone
 */
public class DataValidationException extends RuntimeException {

    private static final long serialVersionUID = 7659985660312721830L;

    public DataValidationException(String message) {
        super(message);
    }

    public DataValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
