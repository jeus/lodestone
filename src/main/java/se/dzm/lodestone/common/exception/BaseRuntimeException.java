package se.dzm.lodestone.common.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */
public abstract class BaseRuntimeException extends RuntimeException  {

    private final String messageCode;
    private final Map<String, Object> params = new HashMap<>();

    public BaseRuntimeException(String messageCode) {
        super(messageCode);
        this.messageCode = messageCode;
    }

    public BaseRuntimeException(String messageCode, Map<String, Object> params) {
        super(messageCode);
        this.messageCode = messageCode;
        this.params.putAll(params);
    }
}