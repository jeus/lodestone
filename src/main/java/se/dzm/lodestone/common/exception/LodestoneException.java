package se.dzm.lodestone.common.exception;

public class LodestoneException extends Exception {

    private static final long serialVersionUID = 1L;

    private LodestoneErrorCode errorCode;

    public LodestoneException(String message, LodestoneErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }


    public LodestoneErrorCode getErrorCode() {
        return errorCode;
    }

}