package se.dzm.lodestone.common.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;
import java.util.Date;

@Slf4j
@RestControllerAdvice
public class LodestoneResponseHandler {


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> handle(Exception exception) {
        log.debug("Processing exception {}", exception.getMessage(), exception);
        ErrorMessage errorMessage = new ErrorMessage();
        try {
            if (exception instanceof LodestoneException) {
                LodestoneException lodestoneException = (LodestoneException) exception;
                errorMessage = handleLodestoneException(lodestoneException);
            }
        } catch (IOException e) {
            log.error("Can't handle exception", e);
        }
        return new ResponseEntity<>(errorMessage, errorMessage.getStatusCode());
    }


    private ErrorMessage handleLodestoneException(LodestoneException lodestoneException) throws IOException {

        LodestoneErrorCode errorCode = lodestoneException.getErrorCode();
        HttpStatus status;

        switch (errorCode) {
            case AUTHENTICATION:
                status = HttpStatus.UNAUTHORIZED;
                break;
            case PERMISSION_DENIED:
                status = HttpStatus.FORBIDDEN;
                break;
            case INVALID_ARGUMENTS:
                status = HttpStatus.BAD_REQUEST;
                break;
            case ITEM_NOT_FOUND:
                status = HttpStatus.NOT_FOUND;
                break;
            case BAD_REQUEST_PARAMS:
                status = HttpStatus.BAD_REQUEST;
                break;
            case GENERAL:
                status = HttpStatus.INTERNAL_SERVER_ERROR;
                break;
            default:
                status = HttpStatus.INTERNAL_SERVER_ERROR;
                break;
        }
        return new ErrorMessage(status, new Date(), lodestoneException.getMessage(),
                lodestoneException.getLocalizedMessage());
    }

}