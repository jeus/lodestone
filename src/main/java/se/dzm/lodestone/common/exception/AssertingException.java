package se.dzm.lodestone.common.exception;

import java.util.Map;

/**
 * @author alikhandani
 * @created 19/06/2022
 * @project lodestone
 */
public class AssertingException extends BaseRuntimeException {

    public AssertingException(String code) {
        super(code);
    }

    public AssertingException(String code, Map<String, Object> params) {
        super(code, params);
    }

}