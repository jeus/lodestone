create EXTENSION IF NOT EXISTS LTREE with SCHEMA public;
create EXTENSION IF NOT EXISTS postgis with SCHEMA public;

create table if not exists company
(
    id integer not null constraint company_pk primary key,
    name varchar(255),
    parent_company_id integer,
    hierarchy ltree
);



create table if not exists station
(
    id integer not null constraint station_pk primary key,
    name varchar(255),
    state varchar(255),
    latitude float8,
    longitude float8,
    company_id integer
);